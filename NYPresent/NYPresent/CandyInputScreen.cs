﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPresent
{
    class CandyInputScreen
    {
        private CandyList ptrCandyList;
        public CandyInputScreen(CandyList ptrCandyList)
        {
            this.ptrCandyList = ptrCandyList;
        }
        public void GetCandy()
        {
            try
            {
                Console.WriteLine();
                Console.WriteLine
                    ("Введите Наименование: ");
                string _name = Console.ReadLine();
                Console.WriteLine
                    ("Введите Цену, Р: ");
                float _price = float.Parse(Console.ReadLine());
                Console.WriteLine
                    ("Введите Вес изделия, кг (0,2): ");
                float _weight = float.Parse(Console.ReadLine());
                Console.WriteLine
                    ("Введите Вкус (Клубника, Банан, Ваниль и т.д.): ");
                string _taste = Console.ReadLine();
                Console.WriteLine
                    ("Введите производителя: ");
                string _firm = Console.ReadLine();
                Console.WriteLine
                    ("Введите Тип (Мармелад, Карамель): ");
                string _type = Console.ReadLine();
                Console.WriteLine
                    ("Спасибо, данные успешно сохранены");
                Candy ptrCandy = new Candy(_name, _price, _weight, _taste,
                    _firm, _type);
                ptrCandyList.InsertCandy(ptrCandy);
                ptrCandyList.Save(_name, _price, _weight, _taste,
                    _firm, _type);
            }
            catch (Exception e)
            { Console.WriteLine(e.Message); }
        }
    }
}
