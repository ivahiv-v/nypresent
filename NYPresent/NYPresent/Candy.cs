﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NYPresent
{
    public class Candy
    {
        private string _name;
        private float _price;
        private float _weight;
        private string _taste;
        private string _firm;
        private string _type;

        public Candy(string _name, float _price, float _weight, string _taste, 
            string _firm, string _type)
        {
            this._name =_name;
            this._price = _price;
            this._weight = _weight;
            this._taste = _taste;
            this._firm = _firm;
            this._type = _type;
        }

        public string Name
        {
            get { return _name; }
            //set { _name = value; }
        }

        public float Price
        {
            get { return _price; }
           // set { _price = value; }
        }

        public float Weight
        {
            get { return _weight; }
           // set { _weight = value; }
        }

        public string Taste
        {
            get { return _taste; }
            //set { _taste = value; }
        }

        public string Firm
        {
            get { return _firm; }
           // set { _firm = value; }
        }

        public string Type
        {
            get { return _type; }
            //set { _type = value; }
        }
    }
}
