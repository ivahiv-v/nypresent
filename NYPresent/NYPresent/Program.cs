﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPresent
{
    sealed class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(150, 40);
            UserInterface ui = new UserInterface();
            ui.Interact();
        }
    }
}
