﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using System.Collections;

namespace NYPresent
{
    sealed class CandyList
    {
        private ArrayList al;
        private const string fName = "CandyList.txt";
        double totalWeight = 0, totalPrice = 0;
        public CandyList()
        {
            al = new ArrayList();
            Load();
        }
        public void InsertCandy(Candy ptrCandy)
        {
            al.Add(ptrCandy);
        }
        public void Display()
        {            
            Console.WriteLine("\nКонфета\t\t\tЦена\t\t\tВес\t\t\tВкус\t\t\tПроизводитель\t\tТип");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------");
            if (al.Count == 0)
                Console.WriteLine();
            else
            {
                foreach (Candy c in al)
                {
                    Console.Write(c.Name.ToString().PadRight(24));
                    Console.Write(c.Price.ToString().PadRight(24));
                    Console.Write(c.Weight.ToString().PadRight(24));
                    Console.Write(c.Taste.ToString().PadRight(24));
                    Console.Write(c.Firm.ToString().PadRight(24));
                    Console.Write(c.Type.ToString().PadRight(24));
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
        }

        public void ChooseCandy()
        {
            Console.WriteLine("\nКонфета\t\t\tЦена\t\t\tВес\t\t\tВкус\t\t\tПроизводитель\t\tТип");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------");
            if (al.Count == 0)
                Console.WriteLine();
            else
            {
                foreach (Candy c in al)
                {
                    Console.Write(c.Name.ToString().PadRight(24));
                    Console.Write(c.Price.ToString().PadRight(24));
                    Console.Write(c.Weight.ToString().PadRight(24));
                    Console.Write(c.Taste.ToString().PadRight(24));
                    Console.Write(c.Firm.ToString().PadRight(24));
                    Console.Write(c.Type.ToString().PadRight(24));
                    Console.WriteLine();
                    if (c.GetType() == typeof(Candy))
                    {
                        totalWeight += c.Weight;
                        totalPrice += c.Price;
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Собранный подарок:\nОбщий вес: " + totalWeight
           + ";\nОбщая цена: " + totalPrice);
                Console.WriteLine();
            }
        }

        public void Load()
        {
            try
            {
                if (!File.Exists(fName))
                    return;
                using (StreamReader sr = new StreamReader(fName))
                {
                    string _name = null;
                    while ((_name = sr.ReadLine()) != null)
                    {
                        float _price = float.Parse(sr.ReadLine());
                        float _weight = float.Parse(sr.ReadLine());
                        string _taste = sr.ReadLine();
                        string _firm = sr.ReadLine();
                        string _type = sr.ReadLine();

                        Candy c =
                            new Candy(_name, _price, _weight, _taste,
                    _firm, _type);
                        InsertCandy(c);
                    }
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void Save(string _name, float _price, float _weight,
            string _taste, string _firm, string _type)
        {
            try
            {
                StreamWriter sw = File.AppendText(fName);
                sw.WriteLine(_name);
                sw.WriteLine(_price);
                sw.WriteLine(_weight);
                sw.WriteLine(_taste);
                sw.WriteLine(_firm);
                sw.WriteLine(_type);
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
