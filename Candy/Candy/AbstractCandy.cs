﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Candy
{
    public interface AbstractCandy
    {
        double Weight
        {
            get;
        }
        double Price
        {
            get;
        }
    }
}
