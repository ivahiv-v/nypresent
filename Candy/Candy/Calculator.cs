﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Candy
{
    public class Calculator
    {
        public void Calc() 
        {
        double totalMarmaladeWeight = 0, totalMarmaladePrice = 0;
            double totalCaramelWeight = 0, totalCaramelPrice = 0;
            double totalChocolateWeight = 0, totalChocolatePrice = 0;
            Present list = new Present();

            foreach (AbstractCandy candy in list.CreateList())
            {
                if (candy.GetType() == typeof(Marmalade))
                {
                    totalMarmaladeWeight += candy.Weight;
                    totalMarmaladePrice += candy.Price;
                }
                if (candy.GetType() == typeof(Caramel))
                {
                    totalCaramelWeight += candy.Weight;
                    totalCaramelPrice += candy.Price;
                }
                else if (candy.GetType() == typeof(Chocolate))
                {
                    totalChocolateWeight += candy.Weight;
                    totalChocolatePrice += candy.Price;
                }
            }
            Console.WriteLine("Подарок:\nВес Мармелада: " + totalMarmaladeWeight
            + ";\nЦена: " + totalMarmaladePrice);
            Console.WriteLine("\nВес Карамели: " + totalCaramelWeight
            + ";\nЦена: " + totalCaramelPrice);
            Console.WriteLine("\nВес Шоколада: " + totalChocolateWeight
            + ";\nЦена: " + totalChocolatePrice);
            Console.ReadKey();
        }
    }
}
