﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Candy
{
     public class Present
    {
         public List<AbstractCandy> CreateList()
         {
             List<AbstractCandy> candyList = new List<AbstractCandy>();

             Marmalade marmalade = new Marmalade();
             marmalade.Name = "Желешка";
             marmalade.Price = 10;
             marmalade.Weight = 0.05;
             Caramel caramel = new Caramel();
             caramel.Name = "Кузнечик";
             caramel.Price = 5;
             caramel.Weight = 0.09;
             Chocolate chocolate = new Chocolate();
             chocolate.Name = "Нестле";
             chocolate.Price = 10;
             chocolate.Weight = 0.05;

             candyList.Add(marmalade);
             candyList.Add(caramel);
             candyList.Add(chocolate);

             return candyList;
         }
    }
}
